package web;

import dao.DaoFactory;
import dao.impl.*;
import dto.UserDTO;
import mapper.BeanMapper;
import model.*;
import service.impl.UserServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        User user =  DaoFactory.getInstance().getUserDao().getBy("login", "vasya");
        UserDTO userDTO = BeanMapper.getInstance().singleMapper(user, UserDTO.class);
        UserServiceImpl.getInstance().upload(userDTO);

    }
}
