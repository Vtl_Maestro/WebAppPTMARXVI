package controllers;

import dao.impl.CrudDAO;
import dao.impl.MovieDaoImpl;
import model.Movie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "FilterServlet", urlPatterns = {"/moviefilter"})
public class FilterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CrudDAO<Movie> movieDao = new MovieDaoImpl(Movie.class);
        MovieDaoImpl movieDaoImpl = new MovieDaoImpl(Movie.class);
        List<Movie> movieListByStartDate = movieDaoImpl.getMovieByStartDate(request.getParameter("start"));
        List<Movie> movieListByEndDate = movieDaoImpl.getMovieByEndDate(request.getParameter("end_date"));
        Movie movieByTitle = movieDaoImpl.getMovieByTitle(request.getParameter("title"));
        request.setAttribute("movieListByStartDate", movieListByStartDate);
        request.setAttribute("movieListByEndDate", movieListByEndDate);
        request.setAttribute("movieByTitle", movieByTitle);
        request.getRequestDispatcher("pages/common/moviefilter.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
