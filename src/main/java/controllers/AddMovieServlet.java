package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;


/**
 * Created by v_minjajlo on 22.07.2016.
 */
@WebServlet(name = "AddMovieServlet", urlPatterns={"/addmovieservlet"})
public class AddMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String title = request.getParameter("title");
        String description = request.getParameter("description");
        int duration = Integer.valueOf(request.getParameter("duration"));
        LocalDate rentStart = LocalDate.parse(request.getParameter("rentStart"));
        LocalDate rentEnd = LocalDate.parse(request.getParameter("rentEnd"));
        String genre = request.getParameter("genre");
        Double rating = Double.valueOf(request.getParameter("rating"));

        MovieDTO movieDTO = new MovieDTO(title, description, duration, rentStart, rentEnd, genre, rating);

        MovieServiceImpl.getInstance().upload(movieDTO);
        request.getRequestDispatcher("pages/common/done.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
