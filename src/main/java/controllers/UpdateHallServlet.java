package controllers;

import dto.HallDTO;
import service.impl.HallServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vitaliy on 30.07.2016.
 */
@WebServlet(name = "UpdateHallServlet", urlPatterns={"/updateHallServlet"})
public class UpdateHallServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("hall_name");
        int id = Integer.valueOf(request.getParameter("id"));

        HallDTO hallDTO = new HallDTO(name);
        hallDTO.setId(id);
        HallServiceImpl.getInstance().update(hallDTO);

        request.getRequestDispatcher("pages/common/done.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
