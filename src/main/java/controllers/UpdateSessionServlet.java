package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Time;
import java.time.LocalDate;

/**
 * Created by Vitaliy on 30.07.2016.
 */
@WebServlet(name = "UpdateSessionServlet", urlPatterns={"/updateSessionServlet"})
public class UpdateSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HallDTO hall = new HallDTO();
        hall.setId(Integer.valueOf(request.getParameter("hall_id")));
        MovieDTO movie = new MovieDTO();
        movie.setId(Integer.valueOf(request.getParameter("movie_id")));
        BigDecimal price = new BigDecimal(request.getParameter("price"));
        Time sessionTime = Time.valueOf(request.getParameter("sessionTime"));
        LocalDate date = LocalDate.parse(request.getParameter("date"));
        int id = Integer.valueOf(request.getParameter("id"));

        SessionDTO sessionDTO = new SessionDTO(hall, movie, price, sessionTime, date);
        sessionDTO.setId(id);
        SessionServiceImpl.getInstance().update(sessionDTO);
        request.getRequestDispatcher("pages/common/done.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
