package controllers;

import dto.SessionDTO;
import dto.TicketDTO;
import dto.UserDTO;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vitaliy on 31.07.2016.
 */
@WebServlet(name = "UpdateTicketServlet", urlPatterns={"/updateTicketservlet"})
public class UpdateTicketServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean isSold = Boolean.parseBoolean(request.getParameter("isSold"));
        int row = Integer.parseInt(request.getParameter("row"));
        int seatNumber = Integer.parseInt(request.getParameter("seatNumber"));
        SessionDTO session = new SessionDTO();
        session.setId(Integer.valueOf(request.getParameter("session")));
        UserDTO user = new UserDTO();
        user.setId(Integer.valueOf(request.getParameter("user")));
        int id = Integer.valueOf(request.getParameter("id"));

        TicketDTO ticketDTO = new TicketDTO(isSold, row, seatNumber, session, user);
        ticketDTO.setId(id);
        TicketServiceImpl.getInstance().update(ticketDTO);

        request.getRequestDispatcher("pages/common/done.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
