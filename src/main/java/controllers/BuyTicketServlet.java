package controllers;

import dto.SessionDTO;
import dto.TicketDTO;
import dto.UserDTO;
import service.impl.TicketServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by v_minjajlo on 28.07.2016.
 */

@WebServlet(name = "BuyTicketServlet", urlPatterns={"/buyticket"})

public class BuyTicketServlet extends HttpServlet {

//    Map<Integer, Integer> rowSeatNumber = new HashMap<>();
    int row;
    int seatNumber;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int[][] place = new int[6][8];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 8; j++) {
                place[i][j] = 0;
                if (request.getParameter(String.valueOf(i)+String.valueOf(j)) != null) {
                    row = i;
                    seatNumber = j;
                    break;
                }
            }
        }

        boolean isSold = true;
        UserDTO user = (UserDTO) request.getSession().getAttribute("user");
        int sissionId = Integer.parseInt(request.getParameter("id"));
        SessionDTO session = new SessionDTO();
        session.setId(sissionId);

        TicketDTO ticketDTO = new TicketDTO(isSold, row, seatNumber, session, user);
        TicketServiceImpl.getInstance().upload(ticketDTO);

        request.getRequestDispatcher("pages/common/ticketbought.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
