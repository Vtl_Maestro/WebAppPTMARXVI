package controllers;

import dto.RowDTO;
import model.Hall;
import service.impl.RowServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vitaliy on 30.07.2016.
 */
@WebServlet(name = "UpdateRowServlet", urlPatterns={"/updateRowservlet"})
public class UpdateRowServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Hall hall = new Hall();
        hall.setId(Integer.valueOf(request.getParameter("hall_id")));
        int rowNumber = Integer.valueOf(request.getParameter("row_number"));
        int seatNumber = Integer.valueOf(request.getParameter("seat_quantity"));
        int id = Integer.valueOf(request.getParameter("id"));

        RowDTO rowDTO = new RowDTO(hall, rowNumber, seatNumber);
        rowDTO.setId(id);
        RowServiceImpl.getInstance().update(rowDTO);
        request.getRequestDispatcher("pages/common/done.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
