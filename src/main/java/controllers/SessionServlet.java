package controllers;

import dao.impl.SessionDaoImpl;
import dto.SessionDTO;
import model.Session;
import service.impl.SessionServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SessionServlet", urlPatterns={"/session"})
public class SessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl(Session.class);
        List<SessionDTO> sessionList = SessionServiceImpl.getInstance().getSessionByMovieId(Integer.valueOf(request.getParameter("movie_id")));
        request.setAttribute("sessionList", sessionList);
        request.getRequestDispatcher("pages/common/session.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
