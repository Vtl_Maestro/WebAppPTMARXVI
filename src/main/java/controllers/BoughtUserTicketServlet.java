package controllers;

import dao.impl.SessionDaoImpl;
import dao.impl.TicketDaoImpl;
import dto.UserDTO;
import model.Session;
import model.Ticket;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by v_minjajlo on 04.08.2016.
 */
@WebServlet(name = "BoughtUserTicketServlet", urlPatterns={"/boughtticket"})

public class BoughtUserTicketServlet extends HttpServlet {
    // Не до конца сделал, не все записи вычитывает из таблицы
    List<Ticket> ticketUserList;
    List<Session> listTicketSession;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserDTO user = (UserDTO) request.getSession().getAttribute("user");
        Integer userId = user.getId();

            ticketUserList = TicketDaoImpl.getInstance().getTicketByUserId(userId);

        for (Ticket ticket: ticketUserList) {
            int sessionId = ticket.getSession().getId();
            listTicketSession = SessionDaoImpl.getInstance().getSessionById(sessionId);
        }
        request.setAttribute("listTicketSession", listTicketSession);
        request.getRequestDispatcher("pages/user/boughtusersticket.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
