package model;

import java.math.BigDecimal;
import java.sql.Time;
import java.time.LocalDate;


public class Session extends Entity<Integer> {
    private Movie movie;
    private Time sessionTime;
    private LocalDate date;
    private Hall hall;
    private BigDecimal price;

    public Session(Hall hall, Movie movie, BigDecimal price, Time sessionTime, LocalDate date) {
        setHall(hall);
        setMovie(movie);
        setPrice(price);
        setSessionTime(sessionTime);
        setDate(date);

    }

    public Session() {
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Time getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(Time sessionTime) {
        this.sessionTime = sessionTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Session session = (Session) o;

        if (getMovie() != null ? !getMovie().equals(session.getMovie()) : session.getMovie() != null) return false;
        if (getSessionTime() != null ? !getSessionTime().equals(session.getSessionTime()) : session.getSessionTime() != null)
            return false;
        if (getHall() != null ? !getHall().equals(session.getHall()) : session.getHall() != null) return false;
        return !(getPrice() != null ? !getPrice().equals(session.getPrice()) : session.getPrice() != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getMovie() != null ? getMovie().hashCode() : 0);
        result = 31 * result + (getSessionTime() != null ? getSessionTime().hashCode() : 0);
        result = 31 * result + (getHall() != null ? getHall().hashCode() : 0);
        result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SessionDTO: ");
        sb.append("hall = ").append(hall).append("\n");
        sb.append(", id = ").append(getId());
        sb.append(", movie = ").append(movie).append("\n");
        sb.append(", sessionTime = ").append(sessionTime);
        sb.append(", sessionDate = ").append(date);
        sb.append(", price = ").append(price);
        return sb.toString();
    }
}
