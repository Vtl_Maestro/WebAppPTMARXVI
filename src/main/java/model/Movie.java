package model;


import java.time.LocalDate;


public class Movie extends Entity<Integer> {

    private String title;
    private String description;
    private int duration;
    private Double rating;
    private String genre;
    private LocalDate rentStart;
    private LocalDate rentEnd;

    public Movie(String title, String description, int duration, LocalDate rentStart, LocalDate rentEnd, String genre, Double rating) {
        this.description = description;
        this.duration = duration;
        this.rentEnd = rentEnd;
        this.genre = genre;
        this.rating = rating;
        this.rentStart = rentStart;
        this.title = title;
    }


    public Movie() {
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Double getRating() {
        return this.rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getGenre() {
        return this.genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public LocalDate getRentStart() {
        return this.rentStart;
    }

    public void setRentStart(LocalDate rentStart) {
        this.rentStart = rentStart;
    }

    public LocalDate getRentEnd() {
        return this.rentEnd;
    }

    public void setRentEnd(LocalDate rentEnd) {
        this.rentEnd = rentEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Movie movie = (Movie) o;

        if (this.duration != movie.duration) return false;
        if (!this.title.equals(movie.title)) return false;
        if (!this.description.equals(movie.description)) return false;
        if (!this.rating.equals(movie.rating)) return false;
        if (!this.genre.equals(movie.genre)) return false;
        if (!this.rentStart.equals(movie.rentStart)) return false;
        return this.rentEnd.equals(movie.rentEnd);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + this.title.hashCode();
        result = 31 * result + this.description.hashCode();
        result = 31 * result + this.duration;
        result = 31 * result + this.rating.hashCode();
        result = 31 * result + this.genre.hashCode();
        result = 31 * result + this.rentStart.hashCode();
        result = 31 * result + this.rentEnd.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                ", rating=" + rating +
                ", genre='" + genre + '\'' +
                ", rentStart=" + rentStart +
                ", rentEnd=" + rentEnd +
                '}';
    }
}
