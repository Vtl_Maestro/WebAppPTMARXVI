package model;


public class Hall extends Entity<Integer> {
    private String name;

    public Hall(String name) {
        setName(name);
    }

    public Hall() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Hall hall = (Hall) o;

        return !(getName() != null ? !getName().equals(hall.getName()) : hall.getName() != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("HallDTO: ");
        sb.append("id = ").append(getId());
        sb.append(", name = '").append(name).append('\'');
        return sb.toString();
    }
}
