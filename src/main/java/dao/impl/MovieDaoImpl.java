package dao.impl;

import datasource.DataSource;
import model.Movie;;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public final class MovieDaoImpl extends CrudDAO<Movie> {
    public static final String INSERT_MOVIE = "INSERT INTO movie(title, description, duration, rent_start, rent_end, genre, rating) VALUES (?, ?, ?, ?, ?, ?, ?)";
    public static final String UPDATE_MOVIE = "UPDATE movie SET title = ?, description = ?, duration = ?, " +
            "rent_start = ?, rent_end = ?, genre = ?, rating = ? WHERE id = ?";
    public static final String UPDATE_MOVIE_TITLE = "UPDATE movie SET title = ? WHERE id = ?";
    public static final String GET_MOVIE_BY_TITLE = "SELECT * FROM movie WHERE title LIKE ?";
    public static final String GET_MOVIE_BY_START = "SELECT * FROM movie WHERE rent_start >= CAST(? AS DATE)";
    public static final String GET_MOVIE_BY_END = "SELECT * FROM movie WHERE rent_start <= CAST(? AS DATE)";
    private static MovieDaoImpl crudDAO;

    public MovieDaoImpl(Class type) {
        super(type);
    }


    public static synchronized MovieDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new MovieDaoImpl(Movie.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Movie entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MOVIE);
        preparedStatement.setString(1, entity.getTitle());
        preparedStatement.setString(2, entity.getDescription());
        preparedStatement.setInt(3, entity.getDuration());
        preparedStatement.setDate(4, Date.valueOf(entity.getRentStart()));
        preparedStatement.setDate(5, Date.valueOf(entity.getRentEnd()));
        preparedStatement.setString(6, entity.getGenre());
        preparedStatement.setDouble(7, entity.getRating());
        preparedStatement.setInt(8, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Movie entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_MOVIE, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getTitle());
        preparedStatement.setString(2, entity.getDescription());
        preparedStatement.setInt(3, entity.getDuration());
        preparedStatement.setDate(4, Date.valueOf(entity.getRentStart()));
        preparedStatement.setDate(5, Date.valueOf(entity.getRentEnd()));
        preparedStatement.setString(6, entity.getGenre());
        preparedStatement.setDouble(7, entity.getRating());
        return preparedStatement;
    }

    @Override
    public List<Movie> readAll(ResultSet resultSet) throws SQLException {
        List<Movie> result = new LinkedList<>();
        Movie movie = null;
        while (resultSet.next()) {
            movie = new Movie();
            movie.setId(resultSet.getInt("id"));
            movie.setTitle(resultSet.getString("title"));
            movie.setDuration(resultSet.getInt("duration"));
            movie.setDescription(resultSet.getString("description"));
            movie.setRating(resultSet.getDouble("rating"));
            movie.setGenre(resultSet.getString("genre"));
            movie.setRentStart(resultSet.getDate("rent_start").toLocalDate());
            movie.setRentEnd(resultSet.getDate("rent_end").toLocalDate());
            result.add(movie);
        }
        return result;
    }

    public void updateMovieTitleById(String title, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MOVIE_TITLE);
            preparedStatement.setString(1, title);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Movie getMovieByTitle(String title) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Movie> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_MOVIE_BY_TITLE);
            preparedStatement.setString(1, title);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
    }

    public List<Movie> getMovieByStartDate(String start) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Movie> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_MOVIE_BY_START);
            preparedStatement.setString(1, start);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Movie movie : result) {
            System.out.println(movie);
        }
        return result;
    }


    public List<Movie> getMovieByEndDate(String end) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Movie> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_MOVIE_BY_END);
            preparedStatement.setString(1, end);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Movie movie : result) {
            System.out.println(movie);
        }
        return result;
    }

}