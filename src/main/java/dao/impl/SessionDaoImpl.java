package dao.impl;

import datasource.DataSource;
import model.Hall;
import model.Movie;
import model.Session;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;


public final class SessionDaoImpl extends CrudDAO<Session> {
    public static final String INSERT_SESSION = "INSERT INTO session (movie_id, time, date, price, hall_id) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE_SESSION = "UPDATE session SET movie_id = ?, time = ?, date = ?, price = ?, hall_id = ? WHERE id = ?";
    public static final String UPDATE_SESSION_TIME = "UPDATE session SET time = ? WHERE id = ?";
    public static final String UPDATE_SESSION_DATE = "UPDATE session SET date = ? WHERE id = ?";
    public static final String UPDATE_SESSION_PRICE = "UPDATE session SET price = ? WHERE id = ?";
    public static final String GET_SESSION_BY_TIME = "SELECT * FROM session WHERE \"time\" = ?";
    public static final String GET_SESSION_BY_DATE = "SELECT * FROM session WHERE \"date\" = CAST(? AS DATE)";
    public static final String GET_SESSION_BY_PRICE = "SELECT * FROM session WHERE price = ?";
    public static final String GET_SESSION_BY_HALL = "SELECT * FROM session WHERE hall_id = ?";
    public static final String GET_SESSION_BY_MOVIE_ID = "SELECT * FROM session WHERE movie_id = ?";
    public static final String GET_SESSION_BY_ID = "SELECT * FROM session WHERE id = ?";

    private static SessionDaoImpl crudDAO;

    public SessionDaoImpl(Class type) {
        super(type);
    }


    public static synchronized SessionDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new SessionDaoImpl(Session.class);
    }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SESSION);
        preparedStatement.setInt(1, entity.getMovie().getId());
        preparedStatement.setTime(2, entity.getSessionTime());
        preparedStatement.setDate(3, Date.valueOf(entity.getDate()));
        preparedStatement.setBigDecimal(4, entity.getPrice());
        preparedStatement.setInt (5, entity.getHall().getId());
        preparedStatement.setInt(6, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SESSION, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getMovie().getId());
        preparedStatement.setTime(2, entity.getSessionTime());
        preparedStatement.setDate(3, Date.valueOf(entity.getDate()));
        preparedStatement.setBigDecimal(4, entity.getPrice());
        preparedStatement.setInt (5, entity.getHall().getId());
        return preparedStatement;
    }

    @Override
    public List<Session> readAll(ResultSet resultSet) throws SQLException {
        List<Session> result = new LinkedList<>();
        Session session = null;
        while (resultSet.next()) {
            session = new Session();
            session.setId(resultSet.getInt("id"));
            session.setMovie(new MovieDaoImpl(Movie.class).getById(resultSet.getInt("movie_id")));
            session.setSessionTime(resultSet.getTime("time"));
            session.setDate(resultSet.getDate("date").toLocalDate());
            session.setPrice(resultSet.getBigDecimal("price"));
            session.setHall(new HallDaoImpl(Hall.class).getById(resultSet.getInt("hall_id")));
            result.add(session);
        }
        return result;
    }

    public void updateSessionTimeById(Time time, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SESSION_TIME);
            preparedStatement.setTime(1, time);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateSessionDateById(LocalDate date, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SESSION_DATE);
            preparedStatement.setDate(1, Date.valueOf(date));
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Session updateSessionPriceById(BigDecimal price, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Session> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SESSION_PRICE);
            preparedStatement.setBigDecimal(1, price);
            preparedStatement.setInt(2, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  result.get(0);
    }

    public List<Session> getSessionByTime (Time time) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Session> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_SESSION_BY_TIME);
            preparedStatement.setTime(1, time);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Session session : result) {
            System.out.println(session);
        }
        return  result;
    }

    public List<Session> getSessionByDate (String date) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Session> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_SESSION_BY_DATE);
            preparedStatement.setString(1, date);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Session session : result) {
            System.out.println(session);
        }
        return  result;
    }

    public List<Session> getSessionBPrice (BigDecimal price) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Session> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_SESSION_BY_PRICE);
            preparedStatement.setBigDecimal(1, price);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Session session : result) {
            System.out.println(session);
        }
        return  result;
    }

    public List<Session> getSessionByHallId (int id) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Session> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_SESSION_BY_HALL);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Session session : result) {
            System.out.println(session);
        }
        return  result;
    }

    public List<Session> getSessionByMovieId (int id) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Session> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_SESSION_BY_MOVIE_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Session session : result) {
            System.out.println(session);
        }
        return  result;
    }
    public List<Session> getSessionById (int id) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Session> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_SESSION_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Session session : result) {
            System.out.println(session);
        }
        return  result;
    }


}
