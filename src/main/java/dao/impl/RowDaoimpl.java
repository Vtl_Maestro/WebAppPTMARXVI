package dao.impl;

import datasource.DataSource;
import model.Hall;
import model.Row;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public final class RowDaoimpl extends CrudDAO<Row> {
    public static final String INSERT_ROW = "INSERT INTO row (row_number, seat_quantity, hall_id) VALUES (?,?,?)";
    public static final String UPDATE_ROW = "UPDATE row SET row_number = ?, seat_quantity = ?, hall_id = ? WHERE id = ?";
    public static final String UPDATE_ROW_SEAT_NUMBER = "UPDATE row SET seat_quantity = ? WHERE id = ?";
    public static final String GET_ROW_BY_ROWNUMBER = "SELECT * FROM row WHERE row_number = ?";
    public static final String GET_ROW_BY_SEATNUMBER = "SELECT * FROM row WHERE seat_quantity = ?";
    public static final String GET_ROW_BY_HALL = "SELECT * FROM row WHERE hall_id = ?";

    private static RowDaoimpl crudDAO;

    public RowDaoimpl(Class type) {
        super(type);
    }


    public static synchronized RowDaoimpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new RowDaoimpl(Row.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Row entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROW);
        preparedStatement.setInt(1, entity.getRowNumber());
        preparedStatement.setInt(2, entity.getSeatNumber());
        preparedStatement.setInt(3, entity.getHall().getId());
        preparedStatement.setInt(4, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Row entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ROW, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getRowNumber());
        preparedStatement.setInt(2, entity.getSeatNumber());
        preparedStatement.setInt(3, entity.getHall().getId());
        return preparedStatement;
    }

    @Override
    public List<Row> readAll(ResultSet resultSet) throws SQLException {
        List<Row> result = new LinkedList<>();
        Row row = null;
        while (resultSet.next()) {
            row = new Row();
            row.setId(resultSet.getInt("id"));
            row.setSeatNumber(resultSet.getInt("seat_quantity"));
            row.setRowNumber(resultSet.getInt("row_number"));
            row.setHall(new HallDaoImpl(Hall.class).getById(resultSet.getInt("hall_id")));
            result.add(row);
        }
        return result;
    }


    public Row getRowByRowNumber (int rowNumber) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Row> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ROW_BY_ROWNUMBER);
            preparedStatement.setInt(1, rowNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
    }

    public Row getRowBySeatNumber (int seatNumber) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Row> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ROW_BY_SEATNUMBER);
            preparedStatement.setInt(1, seatNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
    }

    public Row getRowByHallId (int hallId) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Row> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ROW_BY_HALL);
            preparedStatement.setInt(1, hallId);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
    }


    public void updateRowSeatNumber(int seatNumber, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROW_SEAT_NUMBER);
            preparedStatement.setInt(1, seatNumber);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}