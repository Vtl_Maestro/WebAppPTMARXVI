package dao.impl;

import datasource.DataSource;
import model.Hall;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public final class HallDaoImpl extends CrudDAO<Hall> {
    public static final String INSERT_HALL = "INSERT INTO hall (hall_name) VALUES (?)";
    public static final String UPDATE_HALL = "UPDATE hall SET hall_name = ? WHERE id = ?";
    public static final String GET_HALL_BY_NAME = "SELECT * FROM hall WHERE hall_name LIKE ?";
    public static final String UPDATE_HALL_NAME_BY_ID = "UPDATE hall SET hall_name = ? WHERE id = ?";

    private static HallDaoImpl crudDAO;

    public HallDaoImpl(Class type) {
        super(type);
    }


    public static synchronized HallDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new HallDaoImpl(Hall.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_HALL);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HALL, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getName());
        return preparedStatement;
    }

    @Override
    public List<Hall> readAll(ResultSet resultSet) throws SQLException {
        List<Hall> result = new LinkedList<>();
        Hall hall = null;
        while (resultSet.next()) {
            hall = new Hall();
            hall.setId(resultSet.getInt("id"));
            hall.setName(resultSet.getString("hall_name"));
            result.add(hall);
        }
        return result;
    }


    public Hall getHallByName(String name) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Hall> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_HALL_BY_NAME);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
    }

    public void updateHallNameById(String name, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_HALL_NAME_BY_ID);
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
