package dao.impl;

import datasource.DataSource;
import dto.TicketDTO;
import model.Session;
import model.Ticket;
import model.User;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public final class TicketDaoImpl extends CrudDAO<Ticket> {
    public static final String INSERT_TICKET = "INSERT INTO ticket (session_id, users_id, row_number, seat_number, is_sold) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE_TICKET = "UPDATE ticket SET session_id = ?, users_id = ?, row_number = ?, seat_number = ?, is_sold = ? WHERE id = ?";
    public static final String UPDATE_TICKET_ROW_NUMBER = "UPDATE ticket SET row_number = ? WHERE id = ?";
    public static final String UPDATE_TICKET_SEAT_NUMBER = "UPDATE session SET seat_number = ? WHERE id = ?";
    public static final String GET_TICKET_BY_SESSION_ID = "SELECT * FROM ticket WHERE session_id = ?";
    public static final String GET_TICKET_BY_USER_ID = "SELECT * FROM ticket WHERE users_id = ?";
    public static final String GET_TICKET_BY_ISSOLD = "SELECT * FROM ticket WHERE is_sold = ?";

    private static TicketDaoImpl crudDAO;

    public TicketDaoImpl(Class type) {
        super(type);
    }


    public static synchronized TicketDaoImpl getInstance() {
        if (crudDAO == null) {
            crudDAO = new TicketDaoImpl(Ticket.class);
        }
        return crudDAO;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TICKET);
        preparedStatement.setInt(1, entity.getSession().getId());
        preparedStatement.setInt(2, entity.getUser().getId());
        preparedStatement.setInt(3,entity.getRow());
        preparedStatement.setInt(4,entity.getSeatNumber());
        preparedStatement.setBoolean(5,entity.isSold());
        preparedStatement.setInt(6, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TICKET, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getSession().getId());
        preparedStatement.setInt(2, entity.getUser().getId());
        preparedStatement.setInt(3,entity.getRow());
        preparedStatement.setInt(4,entity.getSeatNumber());
        preparedStatement.setBoolean(5,entity.isSold());
        return preparedStatement;
    }

    @Override
    public List<Ticket> readAll(ResultSet resultSet) throws SQLException {
        List<Ticket> result = new LinkedList<>();
        Ticket ticket = null;
        while (resultSet.next()) {
            ticket = new Ticket();
            ticket.setSession(new SessionDaoImpl(Session.class).getById(resultSet.getInt("session_id")));
            ticket.setUser(new UserDaoImpl(User.class).getById(resultSet.getInt("users_id")));
            ticket.setRow(resultSet.getInt("row_number"));
            ticket.setSeatNumber(resultSet.getInt("seat_number"));
            result.add(ticket);
        }
        return result;
    }

    public void updateTicketRowNumber (int rowNumber, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TICKET_ROW_NUMBER);
            preparedStatement.setInt(1, rowNumber);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateTicketSeatNumber (int seatNumber, Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TICKET_SEAT_NUMBER);
            preparedStatement.setInt(1, seatNumber);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


     public List<Ticket> getTicketBySession (Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Ticket> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_TICKET_BY_SESSION_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Ticket ticket : result) {
            System.out.println(ticket);
        }
        return  result;
    }

    public List<Ticket> getTicketByUserId (Integer id) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Ticket> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_TICKET_BY_USER_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Ticket ticket : result) {
            System.out.println(ticket);
        }
        return  result;
    }

    public List<Ticket> getTicketIsSold (boolean isSold) {
        Connection connection = DataSource.getInstance().getConnection();
        List<Ticket> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_TICKET_BY_ISSOLD);
            preparedStatement.setBoolean(1, isSold);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Ticket ticket : result) {
            System.out.println(ticket);
        }
        return  result;
    }






}
