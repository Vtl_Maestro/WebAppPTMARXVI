package dao;

import dao.api.Dao;
import dao.impl.*;
import helpers.PropertyHolder;
import model.*;

public class DaoFactory {

    private static DaoFactory instance = null;
    private Dao<Integer, Movie> movieDao;
    private Dao<Integer, Hall> hallDao;
    private Dao<Integer, Row> rowDao;
    private Dao<Integer, Role> roleDao;
    private Dao<Integer, Session> sessionDao;
    private Dao<Integer, Ticket> ticketDao;
    private Dao<Integer, User> userDao;

    private DaoFactory(){
        loadDaos();
    }

    public static DaoFactory getInstance(){
        if(instance == null){
            instance = new DaoFactory();
        }
        return instance;
    }

    private void loadDaos() {
       if(PropertyHolder.getInstance().isInMemoryDB()){

       }else{
           movieDao = new MovieDaoImpl(Movie.class);
           userDao = new UserDaoImpl(User.class);
           hallDao = new HallDaoImpl(Hall.class);
           rowDao = new RowDaoimpl(Row.class);
           roleDao = new RoleDaoImpl(Role.class);
           sessionDao = new SessionDaoImpl(Session.class);
           ticketDao = new TicketDaoImpl(Ticket.class);
       }
    }

    public Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }

    public void setMovieDao(Dao<Integer, Movie> movieDao) {
        this.movieDao = movieDao;
    }

    public Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }

    public void setHallDao(Dao<Integer, Hall> hallDao) {
        this.hallDao = hallDao;
    }

    public static void setInstance(DaoFactory instance) {
        DaoFactory.instance = instance;
    }

    public Dao<Integer, Role> getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(Dao<Integer, Role> roleDao) {
        this.roleDao = roleDao;
    }

    public Dao<Integer, Row> getRowDao() {
        return rowDao;
    }

    public void setRowDao(Dao<Integer, Row> rowDao) {
        this.rowDao = rowDao;
    }

    public Dao<Integer, Session> getSessionDao() {
        return sessionDao;
    }

    public void setSessionDao(Dao<Integer, Session> sessionDao) {
        this.sessionDao = sessionDao;
    }

    public Dao<Integer, Ticket> getTicketDao() {
        return ticketDao;
    }

    public void setTicketDao(Dao<Integer, Ticket> ticketDao) {
        this.ticketDao = ticketDao;
    }

    public Dao<Integer, User> getUserDao() {
        return userDao;
    }

    public void setUserDao(Dao<Integer, User> userDao) {
        this.userDao = userDao;
    }
}
