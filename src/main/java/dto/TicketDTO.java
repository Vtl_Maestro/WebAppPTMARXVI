package dto;


public class TicketDTO extends EntityDTO<Integer> {
    private int row;
    private int seatNumber;
    private UserDTO user;
    private boolean isSold;
    private SessionDTO session;

    public TicketDTO(boolean isSold, int row, int seatNumber, SessionDTO session, UserDTO user) {
        this.isSold = isSold;
        this.row = row;
        this.seatNumber = seatNumber;
        this.session = session;
        this.user = user;
    }

    public TicketDTO() {
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public SessionDTO getSession() {
        return session;
    }

    public void setSession(SessionDTO session) {
        this.session = session;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TicketDTO ticket = (TicketDTO) o;

        if (getRow() != ticket.getRow()) return false;
        if (getSeatNumber() != ticket.getSeatNumber()) return false;
        if (isSold() != ticket.isSold()) return false;
        if (getUser() != null ? !getUser().equals(ticket.getUser()) : ticket.getUser() != null) return false;
        return !(getSession() != null ? !getSession().equals(ticket.getSession()) : ticket.getSession() != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getRow();
        result = 31 * result + getSeatNumber();
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        result = 31 * result + (isSold() ? 1 : 0);
        result = 31 * result + (getSession() != null ? getSession().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TicketDTO{");
        sb.append("isSold=").append(isSold);
        sb.append(", row=").append(row);
        sb.append(", seatNumber=").append(seatNumber);
        sb.append(", user=").append(user);
        sb.append(", session=").append(session);
        sb.append('}');
        return sb.toString();
    }
}
