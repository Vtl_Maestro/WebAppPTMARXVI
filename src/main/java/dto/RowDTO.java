package dto;

import model.Entity;
import model.Hall;


public class RowDTO extends Entity<Integer> {
    private int rowNumber;
    private int seatNumber;
    private Hall hall;

    public RowDTO(Hall hall, int rowNumber, int seatNumber) {
        setHall(hall);
        setRowNumber(rowNumber);
        setSeatNumber(seatNumber);
    }

    public RowDTO() {
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RowDTO row = (RowDTO) o;

        if (getRowNumber() != row.getRowNumber()) return false;
        if (getSeatNumber() != row.getSeatNumber()) return false;
        return !(getHall() != null ? !getHall().equals(row.getHall()) : row.getHall() != null);

    }

    @Override
    public int hashCode() {
        int result = getRowNumber();
        result = 31 * result + getSeatNumber();
        result = 31 * result + (getHall() != null ? getHall().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RowDTO: ");
        sb.append("hall = ").append(hall).append("\n");
        sb.append(", rowNumber = ").append(rowNumber);
        sb.append(", seatNumber = ").append(seatNumber);
        return sb.toString();
    }
}
