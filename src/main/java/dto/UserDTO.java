package dto;

import model.Entity;
import java.time.LocalDate;


public class UserDTO extends Entity<Integer> {
        private String login;
        private String password;
        private String firstName;
        private String lastName;
        private String email;
        private String sex;
        private LocalDate birthday;
        private RoleDTO role;

        public UserDTO() {
        }


        public UserDTO(String login, String password, String firstName, String lastName, String email, String sex, LocalDate birthday, RoleDTO role) {
        this.login = login;
        this.role = role;
        this.birthday = birthday;
        this.sex = sex;
        this.email = email;
        this.lastName = lastName;
        this.firstName = firstName;
        this.password = password;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public RoleDTO getRole() {
        return this.role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public LocalDate getBirthday() {
        return this.birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        UserDTO userDTO = (UserDTO) o;

        if (!this.login.equals(userDTO.login)) return false;
        if (!this.password.equals(userDTO.password)) return false;
        if (!this.firstName.equals(userDTO.firstName)) return false;
        if (!this.lastName.equals(userDTO.lastName)) return false;
        if (!this.email.equals(userDTO.email)) return false;
        if (!this.sex.equals(userDTO.sex)) return false;
        if (!this.birthday.equals(userDTO.birthday)) return false;
        return this.role.equals(userDTO.role);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + this.login.hashCode();
        result = 31 * result + this.password.hashCode();
        result = 31 * result + this.firstName.hashCode();
        result = 31 * result + this.lastName.hashCode();
        result = 31 * result + this.email.hashCode();
        result = 31 * result + this.sex.hashCode();
        result = 31 * result + this.birthday.hashCode();
        result = 31 * result + this.role.hashCode();
        return result;
    }


    @Override
    public String toString() {
        return "UserDTO{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday=" + birthday +
                ", role=" + role +
                '}';
    }
}


