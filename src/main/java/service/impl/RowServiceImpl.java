package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.RowDTO;
import mapper.BeanMapper;
import model.Row;
import service.api.Service;

import java.util.List;

public class RowServiceImpl implements Service<Integer, RowDTO> {

    private static RowServiceImpl service;
    private Dao<Integer, Row> rowDao;
    private BeanMapper beanMapper;

    private RowServiceImpl() {
        rowDao = DaoFactory.getInstance().getRowDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized RowServiceImpl getInstance() {
        if (service == null) {
            service = new RowServiceImpl();
        }
        return service;
    }

    @Override
    public List<RowDTO> getAll() {
        List<Row> rows = rowDao.getAll();
        List<RowDTO> rowDTOs = beanMapper.listMapToList(rows, RowDTO.class);
        return rowDTOs;
    }

    @Override
    public void upload(RowDTO rowDTO) {
        Row row = beanMapper.singleMapper(rowDTO, Row.class);
        rowDao.upload(row);
    }

    @Override
    public RowDTO getById(Integer id) {
        Row row = rowDao.getById(id);
        RowDTO rowDTO = beanMapper.singleMapper(row, RowDTO.class);
        return rowDTO;
    }

    //проверить
    @Override
    public void delete(Integer key) {
        rowDao.delete(key);
    }

    //проверить
    @Override
    public void update(RowDTO rowDTO) {
        Row row = beanMapper.singleMapper(rowDTO, Row.class);
        rowDao.update(row);
    }

}
