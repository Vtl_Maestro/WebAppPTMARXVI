package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dao.impl.SessionDaoImpl;
import dto.SessionDTO;
import mapper.BeanMapper;
import model.Session;
import service.api.Service;

import java.util.List;

public class SessionServiceImpl implements Service<Integer, SessionDTO> {

    private static SessionServiceImpl service;
    private Dao<Integer, Session> sessionDao;
    private SessionDaoImpl sessionDaoImpl;
    private BeanMapper beanMapper;

    private SessionServiceImpl() {
        sessionDao = DaoFactory.getInstance().getSessionDao();
        beanMapper = BeanMapper.getInstance();
        sessionDaoImpl = new SessionDaoImpl(Session.class);
    }

    public static synchronized SessionServiceImpl getInstance() {
        if (service == null) {
            service = new SessionServiceImpl();
        }
        return service;
    }

    @Override
    public List<SessionDTO> getAll() {
        List<Session> sessions = sessionDao.getAll();
        List<SessionDTO> sessionDTOs = beanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }

    @Override
    public void upload(SessionDTO sessionDTO) {
        Session session = beanMapper.singleMapper(sessionDTO, Session.class);
        sessionDao.upload(session);
    }

    @Override
    public SessionDTO getById(Integer id) {
        Session session = sessionDao.getById(id);
        SessionDTO sessionDTO = beanMapper.singleMapper(session, SessionDTO.class);
        return sessionDTO;
    }


    @Override
    public void delete(Integer key) {
        sessionDao.delete(key);
    }


    @Override
    public void update(SessionDTO sessionDTO) {
        Session session = beanMapper.singleMapper(sessionDTO, Session.class);
        sessionDao.update(session);
    }

    public List<SessionDTO> getSessionByMovieId(int id) {
        List<Session> session = sessionDaoImpl.getSessionByMovieId(id);
        List<SessionDTO> sessionDTOList = beanMapper.listMapToList(session, SessionDTO.class);
        return sessionDTOList;
    }


}
