package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.TicketDTO;
import mapper.BeanMapper;
import model.Ticket;
import service.api.Service;

import java.util.List;

public class TicketServiceImpl implements Service<Integer, TicketDTO> {

    private static TicketServiceImpl service;
    private Dao<Integer, Ticket> ticketDao;
    private BeanMapper beanMapper;

    private TicketServiceImpl() {
        ticketDao = DaoFactory.getInstance().getTicketDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized TicketServiceImpl getInstance() {
        if (service == null) {
            service = new TicketServiceImpl();
        }
        return service;
    }

    @Override
    public List<TicketDTO> getAll() {
        List<Ticket> tickets = ticketDao.getAll();
        List<TicketDTO> ticketDTOs = beanMapper.listMapToList(tickets, TicketDTO.class);
        return ticketDTOs;
    }

    @Override
    public void upload(TicketDTO ticketDto) {
        Ticket ticket = beanMapper.singleMapper(ticketDto, Ticket.class);
        ticketDao.upload(ticket);
    }

    @Override
    public TicketDTO getById(Integer id) {
        Ticket ticket = ticketDao.getById(id);
        TicketDTO ticketDTO = beanMapper.singleMapper(ticket, TicketDTO.class);
        return ticketDTO;
    }


    @Override
    public void delete(Integer key) {
        ticketDao.delete(key);
    }


    @Override
    public void update(TicketDTO ticketDto) {
        Ticket ticket = beanMapper.singleMapper(ticketDto, Ticket.class);
        ticketDao.update(ticket);
    }

}
