-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cinema_db
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(70) NOT NULL,
  `description` varchar(350) DEFAULT NULL,
  `rent_start` date DEFAULT NULL,
  `rent_end` date DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `genre` varchar(16) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`),
  UNIQUE KEY `description_UNIQUE` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'\"День независимости\"','На Земле принимается сигнал, посланный источником явно неземного происхождения. Источник этот приближается, и на «уши» поднимается вся планета.','1996-06-05','1996-12-10',145,'Боевик',7),(2,'\"Жизнь необычной птицы\"','Сюжет этого австрийского документального фильма рассказывает о самом большом представителе семейства пернатых живущих на нашей планете','2005-07-04','2005-08-10',87,'Документальное',5),(3,'\"Дикая Австралия\"','Сюжет этого сериала можно смело использовать как пособие по выживанию в условиях дикой природы. Зрителям предоставляется уникальное экскурсия по одному из прекрасных и живописных континентов нашей планеты.','2005-07-04','2005-08-10',87,'Документальное',5),(4,'\"Контракт на убийство\"','Главными героями фильма \'\'Контракт на убийство\'\' становятся молодые беззаботные студенты одного из элитных университетов, привыкшие прожигать свою жизнь на всю катушку.','2015-07-04','2015-08-10',124,'Триллер, драма.',11),(5,'\"Последний налет (2016)\"','Эти лихие парни реши завязать с криминалом. Но напоследок нужно урвать солидный куш и он решают пойти на ограбление банка. План безупречен, навар должен быть огромным и если все пройдет гладко это будет их последний налет.','2016-07-04','2016-08-10',117,'Боевик',8),(6,'\"Парадокс (2016)\"','В одной из секретных лабораторий группой передовых ученых в строжайшей секретности ведется создание машины времени. До окончания работы еще далеко, но есть уже значительные сдвиги в работе.','2016-07-04','2016-08-10',134,'Боевик',6),(7,'\"Редукторы (2016)\"','Отец главного героя этого боевика был известным автогонщиком, неоднократно одерживавшим победы в гонках на грунтовых трассах. И парень с детства мечтал пойти по стопам родителя.','2016-07-04','2016-08-10',126,'Боевик',9),(8,'\"Blue Mountain State\"','A Blue Mountain State sorozatot hÃ¡rom Ã©v utÃ¡n rÃ¡hagytÃ¡k, de a szereplÅket sikerÃ¼lt Ã¶sszegyÅ±jteni. Egy rajongÃ³i pÃ©nzbÅl kÃ©szÃ­tett filmhez ismÃ©t Ã¶sszeÃ¡lltak. A diÃ¡kszÃ¶vetsÃ©gi hÃ¡z megmentÃ©sÃ©re rendezett buli kÃ¶rÃ¼l zajlik a film.','2016-05-08','2016-07-15',95,'Vestern',5);
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-05 17:20:34
