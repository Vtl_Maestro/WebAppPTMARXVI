-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cinema_db
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `row`
--

DROP TABLE IF EXISTS `row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `row` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_number` int(11) NOT NULL,
  `seat_quantity` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_row_hall1_idx` (`hall_id`),
  CONSTRAINT `fk_row_hall1` FOREIGN KEY (`hall_id`) REFERENCES `hall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `row`
--

LOCK TABLES `row` WRITE;
/*!40000 ALTER TABLE `row` DISABLE KEYS */;
INSERT INTO `row` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,7,1),(8,2,1,1),(9,2,2,1),(10,2,3,1),(11,2,4,1),(12,2,5,1),(13,2,6,1),(14,2,7,1),(15,3,1,1),(16,3,2,1),(17,3,3,1),(18,3,4,1),(19,3,5,1),(20,3,6,1),(21,3,7,1),(22,4,1,1),(23,4,2,1),(24,4,3,1),(25,4,4,1),(26,4,5,1),(27,4,6,1),(28,4,7,1),(29,5,1,1),(30,5,2,1),(31,5,3,1),(32,5,4,1),(33,5,5,1),(34,5,6,1),(35,5,7,1),(36,1,1,2),(37,1,2,2),(38,1,3,2),(39,1,4,2),(40,1,5,2),(41,1,6,2),(42,1,7,2),(43,2,1,2),(44,2,2,2),(45,2,3,2),(46,2,4,2),(47,2,5,2),(48,2,6,2),(49,2,7,2),(50,3,1,2),(51,3,2,2),(52,3,3,2),(53,3,4,2),(54,3,5,2),(55,3,6,2),(56,3,7,2),(57,4,1,2),(58,4,2,2),(59,4,3,2),(60,4,4,2),(61,4,5,2),(62,4,6,2),(63,4,7,2),(64,5,1,2),(65,5,2,2),(66,5,3,2),(67,5,4,2),(68,5,5,2),(69,5,6,2),(70,5,7,2),(71,1,1,3),(72,1,2,3),(73,1,3,3),(74,1,4,3),(75,1,5,3),(76,1,6,3),(77,1,7,3),(78,2,1,3),(79,2,2,3),(80,2,3,3),(81,2,4,3),(82,2,5,3),(83,2,6,3),(84,2,7,3),(85,3,1,3),(86,3,2,3),(87,3,3,3),(88,3,4,3),(89,3,5,3),(90,3,6,3),(91,3,7,3),(92,4,1,3),(93,4,2,3),(94,4,3,3),(95,4,4,3),(96,4,5,3),(97,4,6,3),(98,4,7,3),(99,5,1,3),(100,5,2,3),(101,5,3,3),(102,5,4,3),(103,5,5,3),(104,5,6,3),(105,5,7,3),(106,5,1,4);
/*!40000 ALTER TABLE `row` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-05 17:20:34
