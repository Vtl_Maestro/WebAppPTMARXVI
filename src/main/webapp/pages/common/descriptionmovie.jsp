<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="head.jsp"></jsp:include>

<div align="center">
    <h3 align="center" style="margin-left: 255px;" >Информация по запрашиваемому Вами фильму:</h3>

    <div align="center" style="margin-left: 445px;" class="show_info">
        <i>Название: </i>${movie.title}
    </div>
    <div align="center" style="margin-left: 445px;" class="show_info">
    <i>Жанр: </i> ${movie.genre}
</div>
    <div align="center" style="margin-left: 445px;" class="show_info">
        <i>Описание: </i>${movie.description}
    </div>
    <div align="center" style="margin-left: 445px;" class="show_info">
        <i>Рейтинг: </i>${movie.rating}
    </div>
    <%--<div align="center" style="margin-left: 445px;" class="show_info">--%>
        <%--<i>Дата начала проката: </i>${movie.rent_start}--%>
    <%--</div>--%>
    <%--<div align="center" style="margin-left: 445px;" class="show_info">--%>
        <%--<i>Дата конца проката: </i>${movie.rent_end}--%>
    <%--</div>--%>
    <div class="show_info">
        <a align="center" style="margin-left: 425px;" href="${pageContext.servletContext.contextPath}/session?movie_id=${movie.id}">Посмотреть сеансы</a>
    </div>
</div>
