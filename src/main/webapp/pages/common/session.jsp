<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="head.jsp"></jsp:include>
<h4 align="center" style="margin-left: 280px;">Для покупки билета и выбора места пожалуйста: <a href="${pageContext.servletContext.contextPath}/pages/common/adduser.jsp">зарегистрируйтесь</a> или <a href="${pageContext.servletContext.contextPath}/pages/user/choicelocation.jsp">авторизируйтесь</a></h4><br/>
<div align="center">
    <h3 align="center" style="margin-left: 250px;" >Cеансы выбранного Вами фильма:</h3>
    <c:forEach var="session" items="${sessionList}">
        <div align="center" style="margin-left: 510px;" style="width: 50%;">
            <div style="float: left">
                <div class="show_info_session">
                    <i>Время: </i>${session.sessionTime}
                </div>
                <div class="show_info_session">
                    <i>Дата: </i>${session.date}
                </div>
                <div class="show_info_session">
                    <i>Цена: </i>${session.price}
                </div>
                <div class="show_info_session">
                    <i>Зал: </i>${session.hall.name}
                </div>
                <div class="show_info_session">
                    <i>Номер сеанса: </i>${session.id}
                </div>
            </div>
        </div>
    </c:forEach>
</div>
 <jsp:include page="mainpage.jsp"></jsp:include>

