<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="headuser.jsp"></jsp:include>
<div align="center">
    <h3 align="center" style="margin-left: 250px;" >Вами приобретены билеты на такие сеансы:</h3>
    <c:forEach var="session" items="${listTicketSession}">
        <div align="center" style="margin-left: 510px;" style="width: 50%;">
            <div style="float: left">
                <div class="show_info_session">
                    <i>Название фильма: </i>${session.movie.title}
                </div>
                <div class="show_info_session">
                    <i>Дата: </i>${session.date}
                </div>
                <div class="show_info_session">
                    <i>Цена: </i>${session.price}
                </div>
                <div class="show_info_session">
                    <i>Зал: </i>${session.hall.name}
                </div>
                <div class="show_info_session">
                    <i>Время: </i>${session.sessionTime}
                </div>
            </div>
        </div>
    </c:forEach>
</div>
