<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="headadmin.jsp"></jsp:include>
<html>
<head>
    <title>AddMovie</title>
</head>
<body>
<h3 align="center" style="margin-left: 300px;">Внесите информацию о фильме:</h3>
<c:out value="${sessionScope.message}"/>
<form align="center" style="margin-left: 550px;" name="addFilmForm" method="post" action="${pageContext.servletContext.contextPath}/addmovieservlet">
    Title: <input align="center" style="margin-left: 5px;" type="text" name="title"/> <br/>
    Description: <input align="center" style="margin-left: 9px;" type="text" name="description"/> <br/>
    Duration: <input align="center" style="margin-left: 37px;"type="int" name="duration"/> <br/>
    RentStart: <input align="center" style="margin-left: 52px;" type="Date" name="rentStart"/> <br/>
    RentEnd: <input align="center" style="margin-left: 31px;" type="Date" name="rentEnd"/> <br/>
    Genre: <input align="center" style="margin-left: 16px;" type="text" name="genre"/> <br/>
    Rating: <input align="center" style="margin-left: 23px;" type="Double" name="rating" /> <br/>
    <input align="center" style="margin-left: 250px;" type="submit" value="Отправить" />
</form>
</body>
</html>
<jsp:include page="../common/mainpage.jsp"></jsp:include>