<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file='headadmin.jsp'%>
<html>
<head>
    <title>Admin</title>
</head>
<body>

<h2 align="center" style="margin-left: 270px;">Страница администратора</h2>

<h3 align="left" style="margin-left: 600px;">Выберите операцию:</br>
    1. <a href="${pageContext.servletContext.contextPath}/pages/admin/addmovie.jsp">Добавить фильм</a> </br>
    2. <a href="${pageContext.servletContext.contextPath}/pages/admin/updatemovie.jsp">Обновить фильм</a> </br>
    3. <a href="${pageContext.servletContext.contextPath}/pages/admin/adduser.jsp">Добавить пользователя</a> </br>
    4. <a href="${pageContext.servletContext.contextPath}/pages/admin/updateuser.jsp">Обновить пользователя</a></br>
    5. <a href="${pageContext.servletContext.contextPath}/pages/admin/addrole.jsp">Добавить роль</a></br>
    6. <a href="${pageContext.servletContext.contextPath}/pages/admin/updaterole.jsp">Обновить роль</a></br>
    7. <a href="${pageContext.servletContext.contextPath}/pages/admin/addsession.jsp">Добавить сессию</a> </br>
    8. <a href="${pageContext.servletContext.contextPath}/pages/admin/updatesession.jsp">Обновить сессию</a></br>
    9. <a href="${pageContext.servletContext.contextPath}/pages/admin/addhall.jsp">Добавить холл</a>  </br>
    10. <a href="${pageContext.servletContext.contextPath}/pages/admin/updatehall.jsp">Обновить холл</a></br>
    11. <a href="${pageContext.servletContext.contextPath}/pages/admin/addrow.jsp">Добавить ряд</a> </br>
    12. <a href="${pageContext.servletContext.contextPath}/pages/admin/updaterow.jsp">Обновить ряд</a></br>
    13. <a href="${pageContext.servletContext.contextPath}/pages/admin/addticket.jsp">Добавить билет</a> </br>
    14. <a href="${pageContext.servletContext.contextPath}/pages/admin/updateticket.jsp">Обновить билет</a></br>
</h3>
</body>
</html>
